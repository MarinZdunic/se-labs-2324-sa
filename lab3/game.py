# ukljucivanje biblioteke pygame
import pygame
import random

pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
# tuple velicine prozora
size = (WIDTH, HEIGHT)
WHITE = [255, 255, 255]
BLACK = [0 , 0, 0]
RED = [255, 0 , 0]
GREEN = [ 0, 255, 0]
BLUE = [0 , 0 ,255]
MAGENTA = [255, 0 , 255]
ORANGE = [255, 88, 0]

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Igra")

myfont =pygame.font.SysFont("Arial",40)
score_text = myfont.render("Score: ", True, RED) #True za ljepsi tekst

clock = pygame.time.Clock()
slikaw = 140
slikah = 100
slika = pygame.image.load("lab3/slika.png")
slika = pygame.transform.scale(slika, [slikaw, slikah])
x = WIDTH/2 - slikaw/2
y = HEIGHT/2 - slikah/2
dirx = 1
diry = 1
vel = 1
bg = ORANGE
score = 0
lives = 5
#States: welcome, game, gameover
state = "welcome"
dt = 0
welcome_screen_time = 3000

done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if state == "gameover":
                if event.key == pygame.K_RETURN:
                    state = "welcome"
                    score = 0
                    lives = 5
                    vel = 1
                    welcome_screen_time = 3000
                    dt = 0
            if event.key == pygame.K_SPACE:
                if bg == ORANGE :
                    bg = MAGENTA
                elif bg ==  MAGENTA:
                    bg = BLUE
                elif bg == BLUE:
                    bg = ORANGE
            if event.key == pygame.K_w:
                vel += 1
            if event.key == pygame.K_s:
                vel -= 1
        if event.type == pygame.MOUSEBUTTONDOWN:
            click = event.pos
            if drawn.collidepoint(click):
                x = random.randint(1, WIDTH-slikaw)
                y = random.randint(1, HEIGHT-slikah)
                vel += 1
                dirx = random.choice([-1,0,1])
                diry = random.choice([-1,0,1]) #0 za gore i dlje
                if dirx == 0 and diry == 0:
                    dirx = random.choice([-1, 1])
                    diry = random.choice([-1, 1])
                score += 1
            else:
                lives -= 1
                
    #Računanje stanja svijeta
    if state == "welcome":
        welcome_screen_time -= dt
        if welcome_screen_time <= 0:
            state = "game"
            
    elif state == "game":
        if lives <= 0:
            state = "gameover" 
                         
        if x >= WIDTH-1-slikaw or x <= 0:
            dirx = dirx * -1
    
        
        if y >= HEIGHT-1-slikah or y <= 0:
            diry = diry * -1 
        
        x += dirx * vel
        y += diry * vel
    elif state == "gameover":
        pass
    
    #Renderiranje
    if state == "welcome":
        screen.fill(GREEN)
        welcome_text = myfont.render("Welcome!", True, RED)
        welocme_time_text = myfont.render(f"{welcome_screen_time/1000}", True, RED)
        screen.blit(welcome_text, [100, 100])
        screen.blit(welocme_time_text, [200, 200])
    
    elif state == "game":
        screen.fill( bg )
        drawn = screen.blit(slika, [x,y])
        score_text = myfont.render(f"Score: {score}", True, WHITE)
        screen.blit(score_text, [20, 20])
        lives_text = myfont.render(f"Lives: {lives}", True, WHITE)
        screen.blit(lives_text, [20, 70])
    elif state == "gameover":
        screen.fill(BLUE)
        gameover_text = myfont.render(f"GAME OVER", True, WHITE)
        score_text = myfont.render(f"Your score is: {score}", True, WHITE)
        screen.blit(gameover_text, [100,100])
        screen.blit(score_text, [200, 200])
    
    pygame.display.flip()

    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    dt = clock.tick(60)

pygame.quit()

