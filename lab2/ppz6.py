
word = input("Unesi rijec ")

reversed = word[::-1]

if word == reversed:
    print(f"Rijec {word} je palindrom")
else:
    print(f"Rijec {word} nije palindrom")