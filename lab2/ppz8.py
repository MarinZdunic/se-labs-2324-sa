def get_player_input():
    player = input("Unesi rock/paper/scissors: ")
    valid_choices = [ "rock", "paper", "scissors" ]
    while player not in valid_choices:
        player = input("Neispravan unos, odaberi rock/paper/scissors: ")
    return player

while True:
    player1 = get_player_input()
    player2 = get_player_input()
    if player1 == player2:
        print("draw")
    elif player1 == "rock":
        if player2 == "paper":
            print("player2")
        elif player2 == "scissors":
            print("player1")
    elif player1 == "paper":
        if player2 == "rock":
            print("player1")
        elif player2 == "scissors":
            print("player2")
    elif player1 == "scissors":
        if player2 == "rock":
            print("player2")
        elif player2 == "paper":
            print("player1")
    
    nastavi = input("nastavi yes/no")
    if nastavi == "no":
        break
