import pygame
import random
import math

class Geometry(object):

    @classmethod
    def get_direction_from_angle(cls, angle, length):
        """ Return x,y direction vector from angle and length"""
        rad_angle = math.radians(angle)
        x = length * math.cos(rad_angle)
        y = length * math.sin(rad_angle)
        return x,y

    @classmethod
    def get_angle_from_line(cls, x1, y1, x2, y2):
        """ Return angle in degrees from two points: 
            
            Params: 
            x1, y1 coordinates for point 1
            x2, y2 coordinates for point 2
        """
        dirx = x2 - x1
        diry = y2 - y1
        rad_angle = math.atan2(diry, dirx)
        deg_angle = rad_angle * 180/math.pi
        return deg_angle % 360


class Player(object):
    """Handles all the logic for the player object"""
    CANNON_COOLDOWN = 10
    TELEPORT_COOLDOWN = 60
    
    def __init__(self, screen, image, bullet_image):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.w, self.h = (130, 70)
        self.x, self.y = self.sw/2, self.sh/2
        self.dx, self.dy = 1,1
        self.dir = [0,0]
        self.vel = 4
        self.angle = 0
        self.d_angle = 2
        self.image = image
        self.drawn = None
        self.bullet_image = bullet_image
        self.bullets = []
        self.cannon_cooldown = self.CANNON_COOLDOWN
        self.teleport_cooldown = self.TELEPORT_COOLDOWN
        

    def update(self):
        mousepos = pygame.mouse.get_pos()
        mousex = mousepos[0]
        mousey = mousepos[1]
        self.angle = Geometry.get_angle_from_line(self.x, self.y, mousex, mousey)
        self.dx, self.dy = Geometry.get_direction_from_angle(self.angle, 1)
        
        self.fire()
        self.teleport()
        
        self.move()
        #self.rect = pygame.Rect(self.x - self.w/2, self.y-self.h/2, self.w, self.h)
    
    def teleport(self):
        self.teleport_cooldown -= 1
        if self.teleport_cooldown <= 0:
            mouse_pressed = pygame.mouse.get_pressed()
            if mouse_pressed[2]:
                mouse_pos = pygame.mouse.get_pos()
                self.x = mouse_pos[0]
                self.y = mouse_pos[1]
                self.teleport_cooldown = self.TELEPORT_COOLDOWN
    
    def fire(self):
        self.cannon_cooldown -= 1
        if self.cannon_cooldown <= 0:
            mouse_pressed = pygame.mouse.get_pressed()
            if mouse_pressed[0]: 
                bullet = Bullet(self.screen, self.bullet_image, self.angle, self.x + 80*self.dx, self.y + 80*self.dy)
                self.bullets.append(bullet)
                self.cannon_cooldown = self.CANNON_COOLDOWN
             

    def draw(self):
        ship_img = pygame.transform.scale(self.image, (self.w, self.h))
        ship_img = pygame.transform.rotate(ship_img, -self.angle)  #kad je pod kutom
        self.rect = ship_img.get_rect(center = (self.x, self.y))
        self.drawn = self.screen.blit(ship_img, self.rect)

    def move(self):
        keys = pygame.key.get_pressed()

        #if keys[pygame.K_a]:
        #    self.angle += self.d_angle
        #elif keys[pygame.K_d]:
        #    self.angle -= self.d_angle
        #self.angle = self.angle % 360

        self.dx, self.dy = Geometry.get_direction_from_angle(self.angle, 1)

        pygame.draw.circle(self.screen, (255,0,0), (int(self.x + self.dx*100), int(self.y + self.dy*100)), 3)

        if keys[pygame.K_w]:
            self.x += self.dx * self.vel
            self.y += self.dy * self.vel
        elif keys[pygame.K_s]:
            self.x -= self.dx * self.vel
            self.y -= self.dy * self.vel



class Asteroid(object):
    """Handles all the logic for the player object"""
    PLAYER_DEAD_ZONE = 300

    def __init__(self, screen, image):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.w = 100
        self.h = 45
        self.image = image
        self.drawn = None
        self.radius = 50
        self.angle = random.randint(0, 359)
        self.init_random_position()
        self.dx, self.dy = Geometry.get_direction_from_angle(self.angle, 1)

    def update(self):
        #self.rect = pygame.Rect(self.x - self.w/2, self.y-self.h/2, self.w, self.h)
        self.move()
        self.angle = Geometry.get_angle_from_line(self.x, self.y, self.x+self.dx, self.y+self.dy)

    def draw(self):
        img = pygame.transform.scale(self.image, [self.w, self.h])
        img = pygame.transform.rotate(img, -self.angle) #- zbog pythona
        self.rect = img.get_rect(center = (self.x, self.y))
        self.drawn = self.screen.blit(img, self.rect)
        
    def init_random_position(self):
        """
        Create x,y coordinates that are at least PLAYER_DEAD_ZONE pixels away
        from the player (center of the screen)."""
        left = random.randint(self.radius, self.sw/2 - self.PLAYER_DEAD_ZONE)
        right = random.randint(self.sw/2+self.PLAYER_DEAD_ZONE, self.sw - self.radius)
        top = random.randint(self.radius, self.sh/2 - self.PLAYER_DEAD_ZONE)
        bottom = random.randint(self.sh/2+self.PLAYER_DEAD_ZONE, self.sh - self.radius)
        self.x = random.choice( [left, right] )
        self.y = random.choice( [top, bottom] )

    def move(self):
        if self.x >= self.sw - self.radius or self.x <= self.radius:
            self.dx *= -1
        if self.y >= self.sh - self.radius or self.y <= self.radius:
            self.dy *= -1
        self.x = self.x + self.dx
        self.y = self.y + self.dy

        
class Bullet(object):
    def __init__(self, screen, image, angle, x, y):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.w = 25
        self.h = 8
        self.image = image
        self.drawn = None
        self.angle = angle
        self.x = x
        self.y = y
        self.vel = 4
        self.dx, self.dy = Geometry.get_direction_from_angle(self.angle, 1)

    def update(self):
        #self.rect = pygame.Rect(self.x - self.w/2, self.y-self.h/2, self.w, self.h)
        self.move()

    def draw(self):
        img = pygame.transform.scale(self.image, [self.w, self.h])
        img = pygame.transform.rotate(img, -self.angle) #- zbog pythona
        self.rect = img.get_rect(center = (self.x, self.y))
        self.drawn = self.screen.blit(img, self.rect)
        
    def move(self):
        #if self.x >= self.sw - self.radius or self.x <= self.radius:
        #    self.dx *= -1
        #if self.y >= self.sh - self.radius or self.y <= self.radius:
        #    self.dy *= -1
        self.x = self.x + self.dx * self.vel
        self.y = self.y + self.dy * self.vel


