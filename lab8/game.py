# ukljucivanje biblioteke pygame
import pygame
import random
from level import Level
from helpers import *

class GameStates:
    WELCOME = 1
    GAME = 2
    GAMEOVER = 3
    LEVELWON = 4


def main():
    
    pygame.font.init()
    # definiranje konstanti za velicinu prozora
    WIDTH = 1280
    HEIGHT = 720
    # tuple velicine prozora
    SIZE = (WIDTH, HEIGHT)
    WHITE = (255,255,255)
    BLACK = (0,0,0)
    RED = (255,0,0)
    GREEN = (0,255,0)
    WELCOME_DURATION = 1500

    BG = load_fig("spacebg1.jpg", SIZE)
    #definiranje novog ekrana za igru
    screen = pygame.display.set_mode(SIZE)
    #definiranje naziva prozora
    pygame.display.set_caption("Asteroids")
    #definiranje sata za pracenje fps-a
    clock = pygame.time.Clock()
    welcome_text = render_text("Watch out for the Asteroids!", 96)

    level = Level(screen) #za iscrtavajne

    states = GameStates
    state = states.WELCOME
    welcome_timeout = WELCOME_DURATION
    dt = 0
    
    done = False
    while not done:
        #event petlja
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True

        #state processing
        if state == states.WELCOME:
            welcome_timeout -= dt
            if welcome_timeout - dt <= 0:
                state = states.GAME
                welcome_timeout = WELCOME_DURATION
                level = Level(screen)

        elif state == states.GAME:
            level.update()
            if level.over:
                state = states.GAMEOVER
            if level.won:
                state = states.LEVELWON    
                
        elif state == states.GAMEOVER:
            welcome_timeout -= dt
            if welcome_timeout - dt <= 0:
                state = states.WELCOME
                welcome_timeout = WELCOME_DURATION
                level = Level(screen)

        #iscrtavanja
        if state == states.WELCOME:
            screen.blit(BG, (0,0))
            screen.blit(welcome_text, center(SIZE, welcome_text))
            timeout = render_text("%.2f"%(welcome_timeout/1000), 48)
            screen.blit( timeout, middle_x(SIZE, timeout, 200))

        elif state == states.GAME:
            screen.blit(BG, (0,0))
            screen.blit(render_text("Score: %d"%level.score,20), (20,20))
            level.draw()
            
        elif state == states.GAMEOVER:
            screen.fill(BLACK)
            gameover_text = render_text("GAME OVER", 80, color = RED)
            screen.blit(gameover_text, center(SIZE,gameover_text))
            
        elif state == states.LEVELWON:
            screen.fill(WHITE)
            text = render_text("LEVEL WON", 80, color = RED)
            screen.blit(text, center(SIZE,text))
          

        pygame.display.flip()
        #ukoliko je potrebno ceka do iscrtavanja 
        #iduceg framea kako bi imao 60fpsa
        dt = clock.tick(60)


if __name__ == "__main__":
    main()
    pygame.quit()
