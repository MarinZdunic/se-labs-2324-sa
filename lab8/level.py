from helpers import *
from entities import Player, Asteroid

class Level(object):
    ASTEROID_COUNT = 10

    SHIP = pygame.image.load("ship_on.png")
    ASTEROID = pygame.image.load("asteroid.png")
    BULLET = pygame.image.load("laser.png")

    def __init__(self, screen):
        self.screen = screen
        self.sw, self.sh = self.screen.get_size()
        
        self.shipimage = self.SHIP
        self.asteroidimage = self.ASTEROID
        self.bulletimage = self.BULLET

        self.player = Player(self)
        self.asteroids = [ Asteroid(self) for i in range(self.ASTEROID_COUNT) ]
        self.bullets = []
        
        self.over = False
        self.won = False
        self.score = 0
        
    def update(self):
        for asteroid in self.asteroids:       
            asteroid.update()
        for bullet in self.bullets:
            bullet.update()
        self.player.update()  
        
        self.check_collisions()
        self.clear_dead_entities()
        
        if len(self.asteroids) == 0:
            self.won = True

    def check_collisions(self):
        for asteroid in self.asteroids:
            for bullet in self.bullets:
                if asteroid.collide_with(bullet):
                    bullet.alive = False
                    asteroid.alive = False
                    self.score += 1
        
        for asteroid in self.asteroids:
            if self.player.collide_with(asteroid):
                asteroid.alive = False
                self.player.aliive = False
                self.over = True
                    
    def clear_dead_entities(self):
        for i, bullet in enumerate(self.bullets):
            if not bullet.alive:
                del self.bullets[i]
        
        for i, asteroid in enumerate(self.asteroids):
            if not asteroid.alive:
                del self.asteroids[i]
                        
    def draw(self):
        for asteroid in self.asteroids:
            asteroid.draw()
        for bullet in self.bullets:
            bullet.draw()
        self.player.draw()
        
        bullets_text = render_text("Bullets: %d"%(len(self.bullets)), 20)
        self.screen.blit(bullets_text, (20, self.sh - 40))

