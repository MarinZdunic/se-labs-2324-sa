<!--- markdown_here --->
# Pismeni ispit 23.02.2023.

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (5 bodova)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u plavu

### 2. Ukloniti teleport funkcionalnost (5 bodova)

- Ukloniti iz koda sve funkcionalnosti povezane s mogućnošću teleportiranja. Osigurati da niste pokidali igru, te da i dalje sve radi ispravno.

### 3. Štit (15 bodova)

- Na slici ispod nalazi se slika štita, u folderu se zove `shield.png`. Omogućiti da se štit prikazuje za cijelo vrijeme iznad playera ako je pritisnuta tipka SPACE.  

<img src="./shield.png" style="width: 200px; height: auto;"/>

- Ovako treba izgledati kada je štit aktivan.

<img src="./screenshot.png" style="width: 600px; height: auto;"/>

### 4. Štit na desni klik (15 bodova)

- onemogućiti prikaz štita na SPACE
- štit se prikazuje na desni klik i aktivan je dok se drži desni klik. 
- aktivacija štita troši energiju koju je potrebno prikazati. Level se započinje sa 600 energije i svaki frame u kojem je štit aktivan energija se smanjuje za 1. To znači da ukupno po levelu možete koristiti štit oko 10 sekundi.
- bilo koji drugi entitet u kontaktu sa štitom se uništava (osim playera)
- Štit se ne prikazuje kada se potroši energija štita

### 5. Punjenje štita (10 bodova)

- Štit kreće sa 100 energije. Svaki protekli frame igre energija štita se puni za 0.1

