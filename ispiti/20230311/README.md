# Pismeni ispit 11.03.2023.

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (5 bodova)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u zelenu

### 2. Healthpack rotation and respawn (15 bodova)

- HealthPack se rotira na mjestu, svaki frame za jedan stupanj
- Pri stvaranju HealthPacka na ekranu, slučajno odabere smjer rotacije
- Svake tri sekunde (180 frameova) HealthPack nestaje sa ekrana i pojavljuje se
  na drugom mjestu

### 3. Bullet Time (10 bodova)

- Dok korisnik drži pritisnutu tipku SPACE, svim bulletima se postavlja brzina 1
- Kada korisnik pusti tipku SPACE, svim bulletima se vraća brzina koju su imali prije bullet time-a

### 4. Teleport (10 bodova)

- Izmjeniti kod za teleportiranje igrača tako da više nema cooldowna, igrač se
  može teleportirati odmah bed čekanja, ali ima ukupno 3 teleportiranja po
  levelu.
- Umjesto cooldown timera staviti tekst koliko je igraču preostalo
  teleportiranja.

### 5. Sliding Turrets (10 bodova)

- Turreti se kreću po x ili y osi, ovisno na kojem dijelu ekrana su se pojavili
- Prate lokaciju igrača
- Vidi skicu ispod - T1 se kreće po X osi, T2 po Y osi
- Oba turreta prate lokaciju igrača, kako se igrač pomiče, tako se i oni pomiću 
po svojim osima


```
-----------T1-------------------
|                              |
|                              |
|                              |
|                              |
|                              |
T2         P                   |
|                              |
|                              |
-------------------------------
```



