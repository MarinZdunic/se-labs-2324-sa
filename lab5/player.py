import random
import pygame

class Player:
    def __init__(self, screen, image_path):
        self.screen = screen
        self.screenw = screen.get_width()
        self.screenh = screen.get_height()
        self.x = random.randint(0, 500)
        self.y = random.randint(0, 500)
        self.dir_x = random.choice( [-1, 0, 1] )
        self.dir_y = random.choice( [-1, 0, 1] )
        self.w = 35
        self.h = 25
        self.vel = 1
        self.old_vel = 0 #sprema prethodno stanje vel, potrebno za stopiranje slika
        self.paused = False
        self.num_jumps = 3
        self.image = pygame.image.load(image_path)
    
    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:   #provjerava je li tipka stisnuta
            if event.key == pygame.K_w:
                self.increase_velocity()
            if event.key == pygame.K_s:
                self.decrease_velocity()
            if event.key == pygame.K_p:
                self.toggle_pause()
            if event.key == pygame.K_r:
                self.jump_to_corner()
                
    def jump_to_corner(self):
        self.num_jumps -= 1
        if self.num_jumps >= 0:
            self.x = random.choice([100, self.screenw - 100 - self.w])
            self.y = random.choice([100, self.screenh - 100 - self.h]) 
                        
    def toggle_pause(self):
        if self.paused:
            #ako je pauziran, odpauziraj
            self.vel = self.old_vel
            self.old_vel = 0 #ova linija nije nuzna
            self.paused = False
        else:
            #ako nije pauziran, pauziraj
            self.old_vel = self.vel
            self.vel = 0
            self.paused = True
                    
    def update_position(self):
        if self.x >= self.screenw - 1 - self.w or self.x <= 0:
            self.dir_x = self.dir_x * -1
        if self.y >= self.screenh - 1 - self.h or self.y <= 0:
            self.dir_y = self.dir_y * -1

        self.x += self.dir_x * self.vel
        self.y += self.dir_y * self.vel

    def draw(self):
        self.scaled = pygame.transform.scale(self.image, [self.w, self.h])
        self.drawn = self.screen.blit(self.scaled, [self.x, self.y])
        
    def check_black_hole(self, hole_drawn):
        touched = self.drawn.colliderect(hole_drawn)
        return touched #vraca jel player dotakao hole

    def clicked(self):
        self.x = random.randint(1, self.screenw - self.w)
        self.y = random.randint(1, self.screenh - self.h)
        self.vel += 1
        self.dir_x = random.choice([-1, 0, 1])
        self.dir_y = random.choice([-1, 0, 1])
        # Ensuring that the image does not stand still
        if self.dir_y == 0 and self.dir_x == 0:
            self.dir_x = random.choice([-1, 1])
            self.dir_y = random.choice([-1, 1])
        self.w += 14
        self.h += 10

    def increase_velocity(self):
        self.vel += 1

    def decrease_velocity(self):
        self.vel -= 1
