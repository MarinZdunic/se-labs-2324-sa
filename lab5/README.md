# Lab 5 - Introduction to Multiple classes in Pygame

**Objective:** Learn the basics of object-oriented programming by incorporating classes into a simple Pygame project.

## Tasks done in Lab 4:

- [x] **Task 1:** Add 2 images moving across the screen in different directions.
  - This will familiarize you with managing multiple objects before introducing classes.

- [x] **Task 2:** Increase the complexity by adding a third image moving in a different direction.
  - Observe how the code starts to get complex and harder to manage.

- [x] **Task 3:** Create a `Player` class that handles the properties and behaviors of an image, such as its position, velocity, and rendering.
  - Ensure that this class has methods to update the object's position and draw it on the screen.

- [x] **Task 4:** Instantiate multiple objects of the `Player` class to replace the individual images.
  - Play with different velocities and starting positions for each object.

## Lab 5 Tasks:

- [ ] **Task 5a:** Split code in multiple class files for better organization.

- [ ] **Task 5b:** Introduce a `Level` class that encapsulates the main game logic, handling score, and managing lives.
  - The `Level` class should create and manage multiple `Player` instances.


- [ ] **Task 6:** Implement collision detection between `Player` instances.
  - Write a method within the `Player` class to detect collisions with other instances.


- [ ] **Task 7:** Enable selecting an image with a mouse click and controlling it with WASD
  - Update the game logic to enable mouse click selection of a Player. 
  - When a player is selected, it stops moving on its own and you can control it with WASD keys.



## Reflection:

- [ ] After completing the tasks, discuss how using classes changed the structure of your code.
  - What benefits did you observe?
  - How did classes help with code organization?
  - What challenges did you face while refactoring the code to use classes?

