# Importing the pygame library
import pygame
import random

# Initialization
pygame.init()
pygame.font.init()

# Defining constants for window size
WIDTH = 1024
HEIGHT = 600
# Tuple for window size
SIZE = (WIDTH, HEIGHT)

# Colors
WHITE = [255, 255, 255]
BLACK = [0, 0, 0]
RED = [255, 0, 0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]
MAGENTA = [255, 0, 255]
ORANGE = [255, 88, 0]

# Creating a new game screen
screen = pygame.display.set_mode(SIZE)

# Setting the window title
pygame.display.set_caption("Moja prva igra")

# Font setup
my_font = pygame.font.SysFont("Arial", 40)
score_text = my_font.render("Score: ", True, RED)
lives_text = my_font.render("Lives: ", True, GREEN)

# Clock for managing frames
clock = pygame.time.Clock()

# Image setup
slika_w = 140
slika_h = 100
slika = pygame.image.load("kohorta.jpg")
slika = pygame.transform.scale(slika, [slika_w, slika_h])

slika_w2 = 140
slika_h2 = 100
slika2 = pygame.image.load("slika.png")
slika2 = pygame.transform.scale(slika2, [slika_w2, slika_h2])

# Player position
x = WIDTH / 2 - slika_w / 2
y = HEIGHT / 2 - slika_h / 2

x2 = WIDTH / 2 - slika_w2 / 2
y2 = HEIGHT / 2 - slika_h2 / 2

# Direction and speed
dir_x = 1
dir_y = 1
vel = 1

dir_x2 = 1
dir_y2 = 1
vel2 = 1

# Game variables
score = 0
lives = 5
state = "welcome"  # states: welcome, game, gameover
dt = 0
welcome_screen_time = 3000

# Background color
bg = ORANGE

# Game loop
done = False
while not done:
    # Event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if state == "gameover":
                if event.key == pygame.K_RETURN:
                    state = "welcome"
                    score = 0
                    lives = 5
                    welcome_screen_time = 3000
                    vel = 1
                    vel = 1
                    dt = 0

            if event.key == pygame.K_SPACE:
                if bg == ORANGE:
                    bg = MAGENTA
                elif bg == MAGENTA:
                    bg = BLUE
                else:
                    bg = ORANGE

            if event.key == pygame.K_w:
                vel += 1
                vel2 += 1
                
            if event.key == pygame.K_s:
                vel -= 1
                vel2 += 1
                

        if event.type == pygame.MOUSEBUTTONDOWN:
            click = event.pos
            # Checking if the image has been clicked
            if drawn.collidepoint(click):
                x = random.randint(1, WIDTH - slika_w)
                y = random.randint(1, HEIGHT - slika_h)
                x2 = random.randint(1, WIDTH - slika_w2)
                y2 = random.randint(1, HEIGHT - slika_h2)
                vel += 1
                vel2 += 1
                dir_x = random.choice([-1, 0, 1])
                dir_y = random.choice([-1, 0, 1])
                dir_x2 = random.choice([-1, 0, 1])
                dir_y2 = random.choice([-1, 0, 1])
                # Ensuring that the image does not stand still
                if dir_y == 0 and dir_x == 0:
                    dir_x = random.choice([-1, 1])
                    dir_y = random.choice([-1, 1])
                score += 1
                if dir_x2 == 0 and dir_x2 == 0:
                    dir_x2 = random.choice([-1, 1])
                    dir_y2 = random.choice([-1, 1])
                score += 1
            else:
                lives -= 1

    # World state calculations
    if state == "welcome":
        welcome_screen_time -= dt
        if welcome_screen_time <= 0:
            state = "game"
    elif state == "game":
        if lives <= 0:
            state = "gameover"

        if x >= WIDTH - 1 - slika_w or x <= 0:
            dir_x = dir_x * -1
        if y >= HEIGHT - 1 - slika_h or y <= 0:
            dir_y = dir_y * -1
        if x2 >= WIDTH - 1 - slika_w2 or x2 <= 0:
            dir_x2 = dir_x2 * -1
        if y2 >= HEIGHT - 1 - slika_h2 or y2 <= 0:
            dir_y2 = dir_y2 * -1
            
        x += dir_x * vel
        y += dir_y * vel
        x2 += dir_x2 * vel
        y2 += dir_y2 * vel

    # Rendering
    if state == "welcome":
        screen.fill(GREEN)
        welcome_text = my_font.render("Welcome!", True, RED)
        welcome_time_text = my_font.render(
            f"{welcome_screen_time / 1000:.2f}", True, RED
        )
        screen.blit(welcome_text, [100, 100])
        screen.blit(welcome_time_text, [200, 200])
    elif state == "game":
        screen.fill(bg)
        drawn = screen.blit(slika, [x, y])
        drawn2 = screen.blit(slika2, [x2, y2])
        score_text = my_font.render(f"Score: {score}", True, WHITE)
        lives_text = my_font.render(f"Lives: {lives}", True, WHITE)
        screen.blit(score_text, [20, 20])
        screen.blit(lives_text, [20, 70])
    elif state == "gameover":
        screen.fill(BLUE)
        gameover_text = my_font.render("Gameover", True, WHITE)
        score_text = my_font.render(f"Score: {score}", True, WHITE)
        screen.blit(gameover_text, [100, 100])
        screen.blit(score_text, [200, 200])

    pygame.display.flip()

    # Frame rate control to achieve 60fps
    dt = clock.tick(60)

# Quitting the game
pygame.quit()
