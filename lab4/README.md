# Lab 4 - Introduction to Classes in Pygame

**Objective:** Learn the basics of object-oriented programming by incorporating classes into a simple Pygame project.

## Tasks:

- [ ] **Task 1:** Add 2 images moving across the screen in different directions.
  - This will familiarize you with managing multiple objects before introducing classes.

- [ ] **Task 2:** Increase the complexity by adding a third image moving in a different direction.
  - Observe how the code starts to get complex and harder to manage.

- [ ] **Task 3:** Create a `GameObject` class that handles the properties and behaviors of an image, such as its position, velocity, and rendering.
  - Ensure that this class has methods to update the object's position and draw it on the screen.

- [ ] **Task 4:** Instantiate multiple objects of the `GameObject` class to replace the individual images.
  - Play with different velocities and starting positions for each object.

- [ ] **Task 5:** Introduce a `Level` class that encapsulates the main game logic, including maintaining the state of the game (e.g., "welcome", "playing", "game over"), handling score, and managing lives.
  - The `Level` class should create and manage multiple `GameObject` instances.

## Advanced Tasks:

- [ ] **Task 6:** Implement collision detection between `GameObject` instances.
  - Write a method within the `GameObject` class to detect collisions with other instances.

- [ ] **Task 7:** Add game states within the `Level` class.
  - Manage transitions between different game states (such as starting the game, ending the game, etc.) inside the `Level` class.

- [ ] **Task 8:** Move score and lives handling from the global scope into the `Level` class.
  - This promotes better organization and encapsulation of game logic.


## Reflection:

- [ ] After completing the tasks, discuss how using classes changed the structure of your code.
  - What benefits did you observe?
  - How did classes help with code organization?
  - What challenges did you face while refactoring the code to use classes?

